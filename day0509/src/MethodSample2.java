public class MethodSample2 {	
	static int method1(boolean b) {
		if (b) {
			return 1;
		}
		return -1;
	}
	
	static void method2(int x) {
		System.out.println(x);
	}
	
	static char method3() {
		return 'a';
	}
	
	static void method4() {
		System.out.println('b');
	}
	
	public static void main(String[] args) {
		int i = method1(true);
		method2(i);
		char c = method3();
		System.out.println(c);
		method4();
	}
}
