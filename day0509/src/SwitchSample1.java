// 12:46～再開します
public class SwitchSample1 {
	public static void main(String[] args) {
		char c = 'b';

		switch (c) {
		case 'a':
		case 'b':
		case 'c':
			System.out.println("ABC");
			break;
		default:
			System.out.println("D");
		}
	}
}
