public interface Soft {
	/**
	 * タイトル画面を表示する
	 */
	void showTitle();

	/**
	 * a ボタンが押された時の処理
	 * @return ゲームを継続するときはfalse 終了するときはtrue
	 */
	boolean pressA();

	/**
	 * b ボタンが押された時の処理
	 * @return ゲームを継続するときはfalse 終了するときはtrue
	 */
	boolean pressB();
}