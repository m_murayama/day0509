
public class EnhancedForLoopSample {
	public static void main(String[] args) {
		String[] names = { "Java", "PHP", "Ruby" };
		for (String name : names) {
			System.out.println(name);
		}
	}
}
