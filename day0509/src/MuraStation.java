import java.util.Scanner;

public class MuraStation {
	Soft soft;

	public void setSoft(Soft soft) {
		this.soft = soft;
	}

	void start() {
		if (this.soft == null) {
			throw new IllegalStateException("The software is not set up.");
		}

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		soft.showTitle();
		loop: while (true) {
			System.out.println("> Please press a, b or q to quit.");
			String line = scanner.nextLine();
			switch (line) {
			case "a":
				if (soft.pressA()) {
					break loop;
				}
				break;
			case "b":
				if (soft.pressB()) {
					break loop;
				}
				break;
			case "q":
				break loop;
			default:
				break;
			}
		}
		System.out.println("Bye!");
	}
}