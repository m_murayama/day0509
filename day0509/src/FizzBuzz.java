// 10:10～再開します
public class FizzBuzz {
	public static void main(String[] args) {
//		・1～30までの数値を出力してください
//		・ただし、3の倍数の場合は Fizz を出力してください
//		・ただし、5の倍数の場合は Buzz を出力してください
//      できたらスクショを送ってください 09:56頃まで
		for (int i = 1; i <= 30; i++) {
			if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("FizzBuzz");
			} else if (i % 3 == 0) {
				System.out.println("Fizz");
			} else if (i % 5 == 0) {
				System.out.println("Buzz");
			} else {
				System.out.println(i);
			}
		}
	}
}
