public class DemoSoft implements Soft {

    @Override
    public void showTitle() {
        System.out.println("DemoSoft");
    }

    @Override
    public boolean pressA() {
        System.out.println("pressA");
        return false;
    }

    @Override
    public boolean pressB() {
        System.out.println("pressB");
        return false;
    }

}