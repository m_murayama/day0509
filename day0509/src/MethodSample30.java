
//           CamelCase
public class MethodSample30 {

	public static void main(String[] args) {

		
		// とりあえずメモしたいときは、//でOK

		/*
		 * 
		 * 
		 * 
		 * 
		 */
		
		/*
		 * 今日は
		 * 火曜日だし
		 * ちょっと
		 * 残って
		 * 勉強して
		 * 変えるとか
		 * かえらんとか
		 */
		
		// リファクタリングとは
		// コードのふるまい（INPUT、OUTPUT）は変えずに、内部構造を改善すること
		int p1 = 100;
		int p2 = 200;

		int total = sum(p1, p2);

		System.out.println(total);
	}

	private static int sum(int price1, int price2) {
		int totalPrice = price1 + price2;
		return totalPrice;
	}

}
