import java.util.Random;

public class Mover implements Soft {

    char[] map = { 'o', '_', '_', '_', '_', '_', '_', '_', '_', '_' };
    int position = 0;

    long startTime;

    @Override
    public void showTitle() {
        System.out.println("============");
        System.out.println("| Mover!!! |");
        System.out.println("============");
        System.out.println();

        show();
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public boolean pressA() {
        clearConsole();
        if (this.position < this.map.length - 1) {
            this.position++;
        }
        if (this.map[this.position] == '|') {
            showGameOver();
            return true;
        }

        setMover();
        setWall();
        show();

        if (this.position == this.map.length - 1) {
            showGoal();
            return true;
        }
        return false;
    }

    @Override
    public boolean pressB() {
        clearConsole();
        if (this.position > 0) {
            this.position--;
        }
        setMover();
        show();
        return false;
    }

    void setMover() {
        for (int i = 0; i < this.map.length; i++) {
            this.map[i] = '_';
        }
        this.map[this.position] = 'o';
    }

    void setWall() {
        Random random = new Random();
        int wallPosition = random.nextInt(this.position, this.map.length);
        if (wallPosition == this.position) {
            if (this.position >= this.map.length - 2) {
                return;
            }
            setWall();
            return;
        }
        this.map[wallPosition] = '|';

    }

    void show() {
        for (int i = 0; i < this.map.length; i++) {
            System.out.print(this.map[i]);
        }
        System.out.println();
    }

    void showGoal() {
        System.out.println("===========");
        System.out.println("| Goal!!! |");
        System.out.println("===========");
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime) + "ms");
    }

    void showGameOver() {
        System.out.println("===========");
        System.out.println("| GameOver |");
        System.out.println("===========");
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime) + "ms");
    }

    void clearConsole() {
    	// Mac
//        System.out.print("\033[H\033[2J");
//        System.out.flush();
    }
}