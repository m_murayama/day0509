package jp.kronos.jdbc.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCSample {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/example";
		String user = "root"; // TODO change user
		String pass = "admin"; // TODO change password
		try (Connection con = DriverManager.getConnection(url, user, pass)) {
			System.out.println("Connection is opened!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
