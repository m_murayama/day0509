
/**
 * メソッドのサンプルを説明するクラス
 * @author Murayama
 *
 */
public class MethodSample1 {

	/**
	 * 足し算します。
	 * @param x 足す数
	 * @param y 足される数
	 * @return 足し算した結果
	 */
	static int add(int x, int y) {
		int result = x + y;
		return result;
	}
	
	public static void main(String[] args) {
		int x = add(10, 20);
		System.out.println(x);
	}
}
