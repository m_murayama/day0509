
public class ForSample2 {
	public static void main(String[] args) {
		for (int i = 0; true; i++) {
			if (i % 2 == 0) {
				System.out.println("Continue");
				continue; // 次のループへ進む
			}
			if (i % 5 == 0) {
				System.out.println("Break");
				break; // ループを終了する
			}
			System.out.println("Hello");
		}
	}
}
